// Lesson Proper
console.log("Hello!...");
console.log('AmerECE');
console.log('pizza');

/* Statements and Syntax
    Statements are instruction/expression we add to our program which will then be communicated to our computers.
    
    Syntax in programming is a set of rules that describes how statement or instructions are properly made or constructed
    line/block of code follow a certain set of rules for it to work appropriately.
*/

console.log('end statement without semicolon')

// let name = "AmerECE";
// let age = 23;
// console.log(name + " " + age);

let num1 = 5;
let num2 = 2;
console.log(num1 + num2);

let num3 = 8;
let num4 = "5";
console.log(num3 + num4);

let myVar;
myVar = "Initialization";
console.log(myVar);

/*
    Not defined and undefined

    not defined is an error. it means your were using a variable that has not been declared

    undefine means that the variable has been declared but there was no initial value

    can we create or declare variable without assigning a initial values?
    yes. however the value of the variable will be undefined.
*/

const myConstant = "Constant Value";
console.log(myConstant);

/*
    myConstant = "New Value";
    constant cannot be changed as same as let
*/
let role = "Manager";
role = "Director";

const tin = "124444-1230";
name = "Juan Dela Cruz";

console.log(`${name} ${role} ${tin}`);

/* Data types
    in most programming languages. data is differentiate into different data types and we can do different things about this data. for most programming language first we have to declare the type of data before we are able to create a variable and store data in it.

    JS dose not require this.
    to create data with particular types, we use literals to create them.
    
    to create strings we use "" or ''
    to create objects we use {}
    to create array we use []
*/

let country = 'Philippines';
let country2 = 'U.S.A';
let country3 = "Japan";
let city = "Rizal";
let city2 = 'California';
let city3 = 'Tokyo';

let philippine = `${city},${country}`;
let usa = `${city2},${country2}`;
let japan = `${city3},${country3}`;

console.log(philippine);
console.log(usa);
console.log(japan);


let num5 = 5.5;
let num6 = .5;
console.log(num5 + num6);

// Boolean
let isAdmin = true;
let isMarried = false;
let isMVP = true;
console.log(`is he admin? ${isAdmin}`);

// Arrays
let array1 = ["Amer", "Orwa", "Dan", "Ala"];
// let array2 = ["One Punch Man", "Saitama", true, 5000];

console.log(array1);
// console.log(array2);

// Objects
let userDetails = {
    userName: "Amer",
    userAge: 23,
    isModerator: true
};

let favorite = {
    firstName: "Dan",
    lastName: "Scot",
    isDeveloper: true,
    age: 32
};
console.log(favorite);

/* Null
    it is explicit absence of data or value. this is done to a project tht a variable contains nothing over undefined as undefined merely mean there is no data in the variable because the variable has not been assigned to an initial value
*/

let sampleNull = null;
let sampleUndefined;

console.log(sampleNull);
console.log(sampleUndefined);

// Functions
function greet () {
    console.log(`Hi`);
};
greet();

function printName (name) {
    console.log(`Hi ${name}`);
};
printName(`AmerECE`);

function showMessage (message) {
    console.log(`${message}`);
};
showMessage(`JavaScript is fun!`);

// Return
// it is used so that the function may return a value.. it also stops the process of the function any other instruction after the keyword will not be process
function fullName (firstName, middleName, lastName) {
    return `${firstName} ${middleName} ${lastName}`
    console.log(`I will no longer run because the function's value/result has been returned`);
};
let fullName1 = fullName("Amer", "Mohammed", "Qasem");
console.log(fullName1);