// Addition
function addition (firstNum, secondNum) {
    console.log(firstNum + secondNum);
};
addition(4, 6);

// Subtraction
function subtraction (firstNum, secondNum) {
    console.log(firstNum - secondNum);
};
subtraction(9, 6);

// Multiplication
function multiplication (firstNum, secondNum) {
    return firstNum * secondNum;
};
let product = multiplication(4, 5);
console.log(product);